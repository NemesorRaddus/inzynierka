from keras.applications.vgg16 import VGG16
from keras.layers import Conv2D, Input, MaxPooling2D, UpSampling2D
from keras.models import Model
from keras.initializers import RandomNormal


def CSRNet(input_shape=(None, None, 1)):

    input_flow = Input(shape=input_shape)
    dilated_conv_kernel_initializer = RandomNormal(stddev=0.01)

    # front-end
    x = Conv2D(4, (3, 3), strides=(1, 1), padding='same', activation='relu')(input_flow)
    x = Conv2D(4, (3, 3), strides=(1, 1), padding='same', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)

    x = Conv2D(8, (3, 3), strides=(1, 1), padding='same', activation='relu')(x)
    x = Conv2D(8, (3, 3), strides=(1, 1), padding='same', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)

    x = Conv2D(16, (3, 3), strides=(1, 1), padding='same', activation='relu')(x)
    x = Conv2D(16, (3, 3), strides=(1, 1), padding='same', activation='relu')(x)
    x = Conv2D(16, (3, 3), strides=(1, 1), padding='same', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)

    x = Conv2D(32, (3, 3), strides=(1, 1), padding='same', activation='relu')(x)
    x = Conv2D(32, (3, 3), strides=(1, 1), padding='same', activation='relu')(x)
    x = Conv2D(32, (3, 3), strides=(1, 1), padding='same', activation='relu')(x)

    # back-end
    x = Conv2D(32, (3, 3), strides=(1, 1), padding='same', dilation_rate=2, activation='relu', kernel_initializer=dilated_conv_kernel_initializer)(x)
    x = Conv2D(32, (3, 3), strides=(1, 1), padding='same', dilation_rate=2, activation='relu', kernel_initializer=dilated_conv_kernel_initializer)(x)
    x = Conv2D(32, (3, 3), strides=(1, 1), padding='same', dilation_rate=2, activation='relu', kernel_initializer=dilated_conv_kernel_initializer)(x)
    x = Conv2D(16, (3, 3), strides=(1, 1), padding='same', dilation_rate=2, activation='relu', kernel_initializer=dilated_conv_kernel_initializer)(x)
    x = Conv2D(8, (3, 3), strides=(1, 1), padding='same', dilation_rate=2, activation='relu', kernel_initializer=dilated_conv_kernel_initializer)(x)
    x = Conv2D(4, (3, 3), strides=(1, 1), padding='same', dilation_rate=2, activation='relu', kernel_initializer=dilated_conv_kernel_initializer)(x)

    x = UpSampling2D((2, 2))(x)
    output_flow = Conv2D(1, 1, strides=(1, 1), padding='same', activation='relu', kernel_initializer=dilated_conv_kernel_initializer)(x)
    model = Model(inputs=input_flow, outputs=output_flow)

    return model