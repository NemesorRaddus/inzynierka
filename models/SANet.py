from tensorflow.keras.layers import Conv2D, Dropout, Input, MaxPooling2D, SeparableConv2D, Conv2DTranspose, Concatenate
from tensorflow_addons.layers import InstanceNormalization
from tensorflow.keras.models import Model


def create_first_SA_module(channels, input, enable_in):
    conv1 = Conv2D(channels, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv1 = InstanceNormalization()(conv1) if enable_in else conv1

    conv3 = Conv2D(channels, (3, 3), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv3 = InstanceNormalization()(conv3) if enable_in else conv3

    conv5 = Conv2D(channels, (5, 5), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv5 = InstanceNormalization()(conv5) if enable_in else conv5

    conv7 = Conv2D(channels, (7, 7), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv7 = InstanceNormalization()(conv7) if enable_in else conv7

    return Concatenate(axis=3)([conv1, conv3, conv5, conv7])


def create_SA_module(channels, input, enable_in):
    conv1 = Conv2D(channels, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv1 = InstanceNormalization()(conv1) if enable_in else conv1

    conv3 = Conv2D(channels * 2, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv3 = InstanceNormalization()(conv3) if enable_in else conv3
    conv3 = Conv2D(channels, (3, 3), padding='same', activation='relu', use_bias=(not enable_in))(conv3)
    conv3 = InstanceNormalization()(conv3) if enable_in else conv3

    conv5 = Conv2D(channels * 2, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv5 = InstanceNormalization()(conv5) if enable_in else conv5
    conv5 = Conv2D(channels, (5, 5), padding='same', activation='relu', use_bias=(not enable_in))(conv5)
    conv5 = InstanceNormalization()(conv5) if enable_in else conv5

    conv7 = Conv2D(channels * 2, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv7 = InstanceNormalization()(conv7) if enable_in else conv7
    conv7 = Conv2D(channels, (7, 7), padding='same', activation='relu', use_bias=(not enable_in))(conv7)
    conv7 = InstanceNormalization()(conv7) if enable_in else conv7

    return Concatenate(axis=3)([conv1, conv3, conv5, conv7])


def SANet(input_shape=(None, None, 1), enable_in=False):
    input = Input(input_shape)

    # FME
    x = create_first_SA_module(16, input, enable_in)
    x = MaxPooling2D()(x)
    x = create_SA_module(32, x, enable_in)
    x = MaxPooling2D()(x)
    x = create_SA_module(32, x, enable_in)
    x = MaxPooling2D()(x)
    x = create_SA_module(32, x, enable_in)

    # DME
    x = Conv2D(64, (9, 9), padding='same', activation='relu', use_bias=(not enable_in))(x)
    x = InstanceNormalization()(x) if enable_in else x
    x = Conv2DTranspose(64, (2, 2), strides=(2, 2), activation='relu')(x)
    x = InstanceNormalization()(x) if enable_in else x

    x = Conv2D(32, (7, 7), padding='same', activation='relu', use_bias=(not enable_in))(x)
    x = InstanceNormalization()(x) if enable_in else x
    x = Conv2DTranspose(32, (2, 2), strides=(2, 2), activation='relu')(x)
    x = InstanceNormalization()(x) if enable_in else x

    x = Conv2D(16, (5, 5), padding='same', activation='relu', use_bias=(not enable_in))(x)
    x = InstanceNormalization()(x) if enable_in else x
    x = Conv2DTranspose(16, (2, 2), strides=(2, 2), activation='relu')(x)
    x = InstanceNormalization()(x) if enable_in else x

    x = Conv2D(16, (3, 3), padding='same', activation='relu', use_bias=(not enable_in))(x)
    x = InstanceNormalization()(x) if enable_in else x
    x = Conv2D(16, (5, 5), padding='same', activation='relu', use_bias=(not enable_in))(x)
    x = InstanceNormalization()(x) if enable_in else x
    x = Conv2D(1, (1, 1), padding='same', activation='linear', use_bias=(not enable_in))(x)

    model = Model(inputs=input, outputs=x, name='SANet')
    return model
