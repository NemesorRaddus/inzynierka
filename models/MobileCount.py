from keras.layers import Conv2D, MaxPooling2D, Concatenate, Input, Dropout, BatchNormalization, Activation, UpSampling2D, Lambda, Add
from keras.models import Model
from keras.backend import reshape

def create_CRP_block(input, out_channels, stages):
    x = input
    top = x
    for _ in range(stages):
        top = MaxPooling2D((5, 5), strides=1, padding='same')(top)
        top = Conv2D(out_channels, (1, 1), use_bias=False)(top)
        x = Concatenate(axis=3)([top, x])
    return x

def create_bottleneck(input, out_channels, strides, downsample, expansion):
    in_channels = input.shape[3]
    intermediate_channels = in_channels * expansion

    x = Conv2D(intermediate_channels, (1, 1), use_bias=False)(input)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    # x = Conv2D(intermediate_channels, (3, 3), strides=strides, padding='same', use_bias=False, groups=intermediate_channels)(x)  # groups somehow not working despite having keras 2.3
    x_grouped = []
    for i in range(intermediate_channels):
        single_channel_x = Lambda(lambda x: x[:, :, :, i:i+1])(x)
        single_channel_conv = Conv2D(1, (3, 3), strides=strides, padding='same', use_bias=False)(single_channel_x)
        x_grouped.append(single_channel_conv)
    x = Concatenate(axis=3)(x_grouped)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = Conv2D(out_channels, (1, 1), use_bias=False)(x)
    x = BatchNormalization()(x)

    if downsample:
        residual = Conv2D(out_channels, (1, 1), strides=strides, use_bias=False)(input)
        residual = BatchNormalization()(residual)
    else:
        residual = input

    x = Add()([x, residual])
    x = Activation('relu')(x)

    return x

def create_encoder_layer(input, num_blocks, out_channels, strides, expansion):
    downsample = strides != 1 or input.shape[3] != out_channels * 4

    x = create_bottleneck(input, out_channels, strides, downsample, expansion)
    for _ in range(1, num_blocks):
        x = create_bottleneck(x, out_channels, 1, False, expansion)

    return x

def MobileCount(input_shape):
    input = Input(input_shape)

    x = Conv2D(32, (3, 3), strides=2, padding='same', use_bias=False)(input)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPooling2D((3, 3), strides=2, padding='same')(x)

    l1 = create_encoder_layer(x, 1, 32, 1, 1)
    l2 = create_encoder_layer(l1, 2, 64, 2, 6)
    l3 = create_encoder_layer(l2, 3, 128, 2, 6)
    l4 = create_encoder_layer(l3, 4, 256, 2, 6)

    l4 = Dropout(0.5)(l4)
    x4 = Conv2D(64, (1, 1), use_bias=False)(l4)
    x4 = Activation('relu')(x4)
    x4 = create_CRP_block(x4, 64, 4)
    x4 = Conv2D(32, (1, 1), use_bias=False)(x4)
    x4 = UpSampling2D((l3.shape[1] // x4.shape[1], l3.shape[2] // x4.shape[2]), interpolation='bilinear')(x4)

    l3 = Dropout(0.5)(l3)
    x3 = Conv2D(32, (1, 1), use_bias=False)(l3)
    x3 = Conv2D(32, (1, 1), use_bias=False)(x3)
    x3 = Add()([x3, x4])
    x3 = Activation('relu')(x3)
    x3 = create_CRP_block(x3, 32, 4)
    x3 = Conv2D(32, (1, 1), use_bias=False)(x3)
    x3 = UpSampling2D((l2.shape[1] // x3.shape[1], l2.shape[2] // x3.shape[2]), interpolation='bilinear')(x3)

    x2 = Conv2D(32, (1, 1), use_bias=False)(l2)
    x2 = Conv2D(32, (1, 1), use_bias=False)(x2)
    x2 = Add()([x2, x3])
    x2 = Activation('relu')(x2)
    x2 = create_CRP_block(x2, 32, 4)
    x2 = Conv2D(32, (1, 1), use_bias=False)(x2)
    x2 = UpSampling2D((l1.shape[1] // x2.shape[1], l1.shape[2] // x2.shape[2]), interpolation='bilinear')(x2)

    x1 = Conv2D(32, (1, 1), use_bias=False)(l1)
    x1 = Conv2D(32, (1, 1), use_bias=False)(x1)
    x1 = Add()([x1, x2])
    x1 = Activation('relu')(x1)
    x1 = create_CRP_block(x1, 32, 4)

    x1 = Dropout(0.5)(x1)
    out = Conv2D(1, (3, 3), padding='same')(x1)
    out = UpSampling2D((input.shape[1] // out.shape[1], input.shape[2] // out.shape[2]), interpolation='bilinear')(out)

    model = Model(inputs=input, outputs=out, name='MobileCount')
    return model
