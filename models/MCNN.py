from keras.layers import Conv2D, MaxPooling2D, Concatenate, Input, SeparableConv2D, Dropout
from keras.models import Model


def MCNN(weights=None, input_shape=(None, None, 1)):
    input_flow = Input(shape=input_shape)
    branches = []

    # large
    f = 16
    k = 9

    x = Conv2D(f, (k, k), padding='same', activation='relu')(input_flow)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(f*2, (k-2, k-2), padding='same', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(f, (k-2, k-2), padding='same', activation='relu')(x)
    x = Conv2D(f//2, (k-2, k-2), padding='same', activation='relu')(x)

    branches.append(x)

    # medium
    f = 20
    k = 7

    x = Conv2D(f, (k, k), padding='same', activation='relu')(input_flow)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(f*2, (k-2, k-2), padding='same', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(f, (k-2, k-2), padding='same', activation='relu')(x)
    x = Conv2D(f//2, (k-2, k-2), padding='same', activation='relu')(x)

    branches.append(x)

    # small
    f = 24
    k = 5

    x = Conv2D(f, (k, k), padding='same', activation='relu')(input_flow)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(f*2, (k-2, k-2), padding='same', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Conv2D(f, (k-2, k-2), padding='same', activation='relu')(x)
    x = Conv2D(f//2, (k-2, k-2), padding='same', activation='relu')(x)

    branches.append(x)

    merged_feature_maps = Concatenate(axis=3)(branches)
    density_map = Conv2D(1, (1, 1), padding='same')(merged_feature_maps)
    model = Model(inputs=input_flow, outputs=density_map)

    return model
