from keras import applications
from keras.layers import Conv2D, Dense, Dropout, Input, AveragePooling2D, Flatten, Reshape, SeparableConv2D
from keras.models import Model


def MobileNet(input_shape=(None, None, 1)):
    input = Input(input_shape)
    x = Conv2D(32, (3, 3), strides=(2, 2), padding='same')(input)
    x = SeparableConv2D(32, (3, 3), strides=(1, 1), padding='same')(x)
    x = SeparableConv2D(64, (3, 3), strides=(2, 2), padding='same')(x)
    x = SeparableConv2D(128, (3, 3), strides=(1, 1), padding='same')(x)
    x = SeparableConv2D(128, (3, 3), strides=(2, 2), padding='same')(x)
    x = SeparableConv2D(256, (3, 3), strides=(1, 1), padding='same')(x)
    x = SeparableConv2D(256, (3, 3), strides=(2, 2), padding='same')(x)
    x = SeparableConv2D(512, (3, 3), strides=(1, 1), padding='same')(x)
    #x = SeparableConv2D(512, (3, 3), strides=(1, 1), padding='same')(x)
    #x = SeparableConv2D(512, (3, 3), strides=(1, 1), padding='same')(x)
    #x = SeparableConv2D(512, (3, 3), strides=(1, 1), padding='same')(x)
    #x = SeparableConv2D(512, (3, 3), strides=(1, 1), padding='same')(x)
    x = SeparableConv2D(32, (3, 3), strides=(2, 2), padding='same')(x)
    x = SeparableConv2D(32, (3, 3), strides=(2, 2), padding='same')(x)
    x = AveragePooling2D((7, 7))(x)
    x = Dense(48 * 64)(x)
    x = Reshape((48, 64, 1))(x)
    model = Model(inputs=input, outputs=x, name='MobileNet')
    return model


def KerasMobileNet(input_shape=(None, None, 1)):
    input_model = applications.MobileNet(input_shape=input_shape, include_top=False, weights=None)
    
    layers = input_model.layers[0:23]
    x = layers[0].output
    for i in range(1, len(layers)):
        x = layers[i](x)

    x = Conv2D(1, (1, 1), padding='same')(x)
    model = Model(inputs=layers[0].input, outputs=x, name='MobileNet')
    return model
