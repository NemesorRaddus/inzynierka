from keras import applications
from keras.layers import Conv2D, UpSampling2D, Dropout
from keras.models import Model


def VGG16(input_shape=(None, None, 1)):
    input_model = applications.VGG16(input_shape=input_shape, include_top=False, weights=None)

    layers = input_model.layers[0:23]
    x = layers[0].output
    for i in range(1, len(layers)):
        x = layers[i](x)

    x = Conv2D(128, (1, 1), padding='same', activation='relu')(x)
    x = Dropout(.5, seed=31415)(x)
    x = Conv2D(1, (1, 1), padding='same', activation='relu')(x)
    x = UpSampling2D((8, 8), interpolation='bilinear')(x)
    x = Conv2D(1, (1, 1), padding='same')(x)
    model = Model(inputs=layers[0].input, outputs=x, name='VGG16')
    return model
