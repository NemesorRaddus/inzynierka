from tensorflow.keras.layers import Conv2D, Dropout, Input, MaxPooling2D, SeparableConv2D, Conv2DTranspose, Concatenate
from tensorflow_addons.layers import InstanceNormalization
from tensorflow.keras.models import Model


def create_first_SA_moduleDS(channels, input, enable_in, ds_ratio):
    channels_conv = round(channels * (1 - ds_ratio))
    channels_ds = round(channels * ds_ratio)

    conv1 = Conv2D(channels_conv, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv1 = InstanceNormalization()(conv1) if enable_in else conv1

    conv1ds = SeparableConv2D(channels_ds, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv1ds = InstanceNormalization()(conv1ds) if enable_in else conv1ds

    conv3 = Conv2D(channels_conv, (3, 3), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv3 = InstanceNormalization()(conv3) if enable_in else conv3

    conv3ds = SeparableConv2D(channels_ds, (3, 3), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv3ds = InstanceNormalization()(conv3ds) if enable_in else conv3ds

    conv5 = Conv2D(channels_conv, (5, 5), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv5 = InstanceNormalization()(conv5) if enable_in else conv5

    conv5ds = SeparableConv2D(channels_ds, (5, 5), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv5ds = InstanceNormalization()(conv5ds) if enable_in else conv5ds

    conv7 = Conv2D(channels_conv, (7, 7), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv7 = InstanceNormalization()(conv7) if enable_in else conv7

    conv7ds = SeparableConv2D(channels_ds, (7, 7), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv7ds = InstanceNormalization()(conv7ds) if enable_in else conv7ds

    return Concatenate(axis=3)([conv1, conv1ds, conv3, conv3ds, conv5, conv5ds, conv7, conv7ds])


def create_SA_moduleDS(channels, input, enable_in, ds_ratio):
    channels_conv = round(channels * (1 - ds_ratio))
    channels_ds = round(channels * ds_ratio)

    conv1 = Conv2D(channels_conv, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv1 = InstanceNormalization()(conv1) if enable_in else conv1

    conv1ds = SeparableConv2D(channels_ds, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv1ds = InstanceNormalization()(conv1ds) if enable_in else conv1ds

    conv3 = Conv2D(channels_conv * 2, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv3 = InstanceNormalization()(conv3) if enable_in else conv3
    conv3 = Conv2D(channels_conv, (3, 3), padding='same', activation='relu', use_bias=(not enable_in))(conv3)
    conv3 = InstanceNormalization()(conv3) if enable_in else conv3

    conv3ds = SeparableConv2D(channels_ds * 2, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv3ds = InstanceNormalization()(conv3ds) if enable_in else conv3ds
    conv3ds = SeparableConv2D(channels_ds, (3, 3), padding='same', activation='relu', use_bias=(not enable_in))(conv3ds)
    conv3ds = InstanceNormalization()(conv3ds) if enable_in else conv3ds

    conv5 = Conv2D(channels_conv * 2, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv5 = InstanceNormalization()(conv5) if enable_in else conv5
    conv5 = Conv2D(channels_conv, (5, 5), padding='same', activation='relu', use_bias=(not enable_in))(conv5)
    conv5 = InstanceNormalization()(conv5) if enable_in else conv5

    conv5ds = SeparableConv2D(channels_ds * 2, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv5ds = InstanceNormalization()(conv5ds) if enable_in else conv5ds
    conv5ds = SeparableConv2D(channels_ds, (5, 5), padding='same', activation='relu', use_bias=(not enable_in))(conv5ds)
    conv5ds = InstanceNormalization()(conv5ds) if enable_in else conv5ds

    conv7 = Conv2D(channels_conv * 2, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv7 = InstanceNormalization()(conv7) if enable_in else conv7
    conv7 = Conv2D(channels_conv, (7, 7), padding='same', activation='relu', use_bias=(not enable_in))(conv7)
    conv7 = InstanceNormalization()(conv7) if enable_in else conv7

    conv7ds = SeparableConv2D(channels_ds * 2, (1, 1), padding='same', activation='relu', use_bias=(not enable_in))(input)
    conv7ds = InstanceNormalization()(conv7ds) if enable_in else conv7ds
    conv7ds = SeparableConv2D(channels_ds, (7, 7), padding='same', activation='relu', use_bias=(not enable_in))(conv7ds)
    conv7ds = InstanceNormalization()(conv7ds) if enable_in else conv7ds

    return Concatenate(axis=3)([conv1, conv1ds, conv3, conv3ds, conv5, conv5ds, conv7, conv7ds])


def SANetDS(ds_to_conv, input_shape=(None, None, 1), enable_in=False):
    ds_ratio = ds_to_conv[0] / sum(ds_to_conv)
    channels64_conv = round(64 * (1 - ds_ratio))
    channels64_ds = round(64 * ds_ratio)
    channels32_conv = round(32 * (1 - ds_ratio))
    channels32_ds = round(32 * ds_ratio)
    channels16_conv = round(16 * (1 - ds_ratio))
    channels16_ds = round(16 * ds_ratio)

    input = Input(input_shape)

    # FME
    x = create_first_SA_moduleDS(16, input, enable_in, ds_ratio)
    x = MaxPooling2D()(x)
    x = create_SA_moduleDS(32, x, enable_in, ds_ratio)
    x = MaxPooling2D()(x)
    x = create_SA_moduleDS(32, x, enable_in, ds_ratio)
    x = MaxPooling2D()(x)
    x = create_SA_moduleDS(16, x, enable_in, ds_ratio)

    # DME
    xconv = Conv2D(channels64_conv, (9, 9), padding='same', activation='relu', use_bias=(not enable_in))(x)
    xds = SeparableConv2D(channels64_ds, (9, 9), padding='same', activation='relu', use_bias=(not enable_in))(x)
    x = Concatenate(axis=3)([xconv, xds])
    x = InstanceNormalization()(x) if enable_in else x
    x = Conv2DTranspose(64, (2, 2), strides=(2, 2), activation='relu')(x)
    x = InstanceNormalization()(x) if enable_in else x

    xconv = Conv2D(channels32_conv, (7, 7), padding='same', activation='relu', use_bias=(not enable_in))(x)
    xds = SeparableConv2D(channels32_ds, (7, 7), padding='same', activation='relu', use_bias=(not enable_in))(x)
    x = Concatenate(axis=3)([xconv, xds])
    x = InstanceNormalization()(x) if enable_in else x
    x = Conv2DTranspose(32, (2, 2), strides=(2, 2), activation='relu')(x)
    x = InstanceNormalization()(x) if enable_in else x

    xconv = Conv2D(channels16_conv, (5, 5), padding='same', activation='relu', use_bias=(not enable_in))(x)
    xds = SeparableConv2D(channels16_ds, (5, 5), padding='same', activation='relu', use_bias=(not enable_in))(x)
    x = Concatenate(axis=3)([xconv, xds])
    x = InstanceNormalization()(x) if enable_in else x
    x = Conv2DTranspose(16, (2, 2), strides=(2, 2), activation='relu')(x)
    x = InstanceNormalization()(x) if enable_in else x

    xconv = Conv2D(channels16_conv, (3, 3), padding='same', activation='relu', use_bias=(not enable_in))(x)
    xds = SeparableConv2D(channels16_ds, (3, 3), padding='same', activation='relu', use_bias=(not enable_in))(x)
    x = Concatenate(axis=3)([xconv, xds])
    x = InstanceNormalization()(x) if enable_in else x
    xconv = Conv2D(channels16_conv, (5, 5), padding='same', activation='relu', use_bias=(not enable_in))(x)
    xds = SeparableConv2D(channels16_ds, (5, 5), padding='same', activation='relu', use_bias=(not enable_in))(x)
    x = Concatenate(axis=3)([xconv, xds])
    x = InstanceNormalization()(x) if enable_in else x
    x = Conv2D(1, (1, 1), padding='same', activation='linear', use_bias=(not enable_in))(x)

    model = Model(inputs=input, outputs=x, name='SANet')
    return model
