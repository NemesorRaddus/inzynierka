import matplotlib.pyplot as plt
import os
import pandas as pd
import shutil

from keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard


def prepare_output_dirs():
    if not os.path.exists('logs'):
        os.path.makedirs('logs')
    if not os.path.exists('results'):
        os.path.makedirs('results')
    if not os.path.exists('weights'):
        os.path.makedirs('weights')

    # Remove previous tensorboard data
    if os.path.exists('logs/train/'):
        shutil.rmtree('logs/train/')

    if os.path.exists('logs/validation/'):
        shutil.rmtree('logs/validation/')

        
def create_weights_callbacks(weights_train_path, weights_val_path):
    checkpointer_best_val = ModelCheckpoint(
        filepath=weights_val_path,
        monitor='val_loss', verbose=1, save_best_only=True, mode='min'
    )

    checkpointer_best_train = ModelCheckpoint(
        filepath=weights_train_path,
        monitor='loss', verbose=1, save_best_only=True, mode='min'
    )

    return [checkpointer_best_val, checkpointer_best_train]


def create_tensorboard_callback():
    return TensorBoard(log_dir='logs')


def create_early_stopping_callback(patience = 25):
    return EarlyStopping(
        monitor="val_loss",
        min_delta=0,
        patience=patience,
        verbose=1,
        mode="auto",
        baseline=None,
        restore_best_weights=False,
    )


def read_history(history):
    return history.history['loss'], history.history['val_loss']


def plot_losses(loss, val_loss, ylim=None):
    plt.plot(loss, 'b')
    plt.plot(val_loss, 'r')
    if ylim:
        plt.gca().set_ylim(ylim)
    plt.legend(['loss', 'val_loss'])


def save_losses(loss, val_loss, res_name, save_plot=True):
    if save_plot:
        plt.savefig(os.path.join('results', f'{res_name}.png'))

    csv_path = os.path.join('results', f'{res_name}_train.csv')
    pd.DataFrame({'loss': loss, 'val_loss': val_loss}).to_csv(csv_path, index=False)
