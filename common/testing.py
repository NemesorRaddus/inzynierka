import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import tqdm


def get_counts(density_maps):
    counts = []
    for dm in density_maps:
        counts.append(np.sum(dm))
    return counts


def predict_counts(model, images, return_density_maps=False):
    counts = []
    density_maps = []

    for img in tqdm.tqdm(images):
        inputs = np.reshape(img, [1, *img.shape])
        outputs = np.squeeze(model.predict(inputs))
        counts.append(get_counts([outputs])[0])
        if return_density_maps:
            density_maps.append(outputs)

    if return_density_maps:
        return counts, density_maps
    else:
        return counts


def get_mae_rmse(target_counts, predicted_counts):
    avg_errors = []
    sq_errors = []
    for target, prediction in zip(target_counts, predicted_counts):
        avg_errors.append(abs(target - prediction))
        sq_errors.append((target - prediction) ** 2)

    mae = np.mean(avg_errors)
    rmse = np.sqrt(np.mean(sq_errors))

    return mae, rmse


def save_test_results(model, mae, rmse, res_name, train_or_val_weights, weights_path):
    csv_path = os.path.join('results', f'{res_name}_test_{train_or_val_weights}.csv')
    pd.DataFrame({'MAE': [mae], 'RMSE': [rmse]}).to_csv(csv_path, index=False)

    if mae < 50:
        model.save(weights_path.replace('.', f'_{str(int(mae))}_{str(int(rmse))}.'))


def plot_prediction(image, target_count, target_dm, predicted_count, predicted_dm):
    fg, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=(16, 5))

    plt.suptitle(' '.join([
        'target_count:', str(round(target_count, 3)),
        'predicted_count:', str(round(predicted_count, 3))
    ]))

    ax0.imshow(np.squeeze(image), cmap='gray')
    ax1.imshow(target_dm * (255 / (np.max(target_dm) - np.min(target_dm))), cmap='gray')
    ax2.imshow(predicted_dm * (255 / (np.max(predicted_dm) - np.min(predicted_dm))), cmap='gray')

    plt.show()
