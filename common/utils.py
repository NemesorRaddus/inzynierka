import tensorflow as tf
import keras

from keras.models import load_model
from tensorflow.python.client import device_lib


def check_GPU():
    print(device_lib.list_local_devices())


# ref: https://stackoverflow.com/questions/49525776/how-to-calculate-a-mobilenet-flops-in-keras
def get_flops(weights_val_path, custom_objects):
    session = tf.compat.v1.Session()
    graph = tf.compat.v1.get_default_graph()

    with graph.as_default():
        with session.as_default():
            model = load_model(weights_val_path, custom_objects)

            run_meta = tf.compat.v1.RunMetadata()

            opts = tf.compat.v1.profiler.ProfileOptionBuilder.float_operation()
            flops = tf.compat.v1.profiler.profile(graph=graph, run_meta=run_meta, cmd='op', options=opts)
            
    tf.compat.v1.reset_default_graph()

    return flops.total_float_ops


def print_flops(flops, model):
    print(f'FLOPS: {flops:,}')
    print(f'Mult-Adds: {int(flops / 2):,}')
    print(f'Trainable params: {model.count_params():,}')
