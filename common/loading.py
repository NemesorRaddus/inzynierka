import numpy as np
import os
import pickle
import random

from common import constants


def _load_data(fileName):
    return pickle.load(open(os.path.join(constants.NPCACHE_ROOT, fileName), 'rb'))


def load_training_validation_data(part):
    train_img = _load_data(f'part_{part}_train_img.npy')
    print('Loaded training images')

    train_dm = _load_data(f'part_{part}_train_den.npy')
    print('Loaded training density maps')

    validation_img = _load_data(f'part_{part}_val_img.npy')
    print('Loaded validation images')

    validation_dm = _load_data(f'part_{part}_val_den.npy')
    print('Loaded validation density maps')

    return train_img, train_dm, validation_img, validation_dm


def load_test_data(part):
    test_img = _load_data(f'part_{part}_test_img.npy')
    print('Loaded test images')

    test_dm = _load_data(f'part_{part}_test_den.npy')
    print('Loaded test density maps')

    return test_img, test_dm


def _shuffle_data_with_respect_to_sizes(imgs, labels):
    groups = []

    prev_img_size = imgs[0].shape
    prev_lab_size = labels[0].shape
    group = []

    for img, label in zip(imgs, labels):
        if prev_img_size != img.shape or prev_lab_size != label.shape:
            random.shuffle(group)
            groups.append(group)
            prev_img_size = img.shape
            prev_lab_size = label.shape
            group = []
        group.append((img, label))
    groups.append(group)

    random.shuffle(groups)

    imgs, labels = [], []
    for g in groups:
        g_imgs, g_labels = zip(*g)
        imgs += g_imgs
        labels += g_labels

    return imgs, labels


# imgs and labels should be sorted by shape
def create_generator(imgs, labels, max_batch_size=1, loop_infinitely=True, shuffle=True):
    total_len = min(len(imgs), len(labels))
    while True:
        if shuffle:
            imgs, labels = _shuffle_data_with_respect_to_sizes(imgs, labels)

        if max_batch_size == 1:
            for i in range(total_len):
                yield (np.array([imgs[i]]).reshape((1, *imgs[i].shape)), np.array([labels[i]]).reshape((1, *labels[i].shape)))
        else:
            current = 0
            generated_img = []
            generated_lab = []
            prev_img_size = imgs[0].shape
            prev_lab_size = labels[0].shape
            for i in range(total_len):
                if prev_img_size != imgs[i].shape or prev_lab_size != labels[i].shape:
                    yield (np.array(generated_img).reshape((current, *prev_img_size)), np.array(generated_lab).reshape((current, *prev_lab_size)))
                    current = 0
                    generated_img = []
                    generated_lab = []
                    prev_img_size = imgs[i].shape
                    prev_lab_size = labels[i].shape
                generated_img.append(imgs[i])
                generated_lab.append(labels[i])
                current += 1
                if current >= max_batch_size:
                    yield (np.array(generated_img).reshape((current, *prev_img_size)), np.array(generated_lab).reshape((current, *prev_lab_size)))
                    current = 0
                    generated_img = []
                    generated_lab = []
            if current != 0:
                yield (np.array(generated_img).reshape((current, *prev_img_size)), np.array(generated_lab).reshape((current, *prev_lab_size)))
        if not loop_infinitely:
            break


def get_steps_per_epoch(imgs, labels, max_batch_size):
    cnt = 0
    for batch in create_generator(imgs, labels, max_batch_size, False):
        cnt += 1
    return cnt
